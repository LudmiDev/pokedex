import { getDatabase, ref, onValue } from 'firebase/database';

export default function readInFirebase(user, category) {
  const db = getDatabase();
  const reference = ref(db, `${user}/${category}`);
  onValue(reference, (snapshot) => {
    const data = snapshot.val();
    return data;
  });
}
