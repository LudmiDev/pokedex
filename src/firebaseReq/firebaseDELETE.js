import { getDatabase, ref, remove } from 'firebase/database';

function firebaseDELETE(userID, category) {
  const db = getDatabase();
  remove(ref(db, `users/${userID}/${category}/`));
}
export default firebaseDELETE;
