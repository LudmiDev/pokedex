import {
  getDatabase, ref, child, get,
} from 'firebase/database';

const dbRef = ref(getDatabase());

export async function firebaseGET(userID, category) {
  const result = await get(child(dbRef, `${userID}/${category}`))
    .then((snapshot) => snapshot.val())
    .catch((error) => {
      console.error(error);
    });
  return result;
}

export async function firebaseGetOne(userID, category, ID) {
  const result = await get(child(dbRef, `${userID}/${category}/${ID}`))
    .then((snapshot) => snapshot.val())
    .catch((error) => {
      console.error(error);
    });
  return result;
}
