/* eslint-disable no-return-await */
import React, { useState, useEffect } from 'react';
import { Route, Routes } from 'react-router-dom';
import Protected from './Components/Protected/Protected';
import { AuthContextProvider } from './Context/AuthContext';
import Account from './Pages/Account/Account';
import { getId, getPokemonDetail, getRegion } from './Components/PokemonAPI/api';
import HomeLogged from './Pages/HomeLogged/HomeLogged';
import Home from './Pages/Home/Home';
import Team from './Components/Teams/Team';
import LogIn from './Pages/LogIn/LogIn';
import SignUp from './Pages/SignUp/SignUp';
import CustomTeam from './Components/CustomTeam/CustomTeam';

function App() {
  const [pokemons, setPokemons] = useState([]);
  const [total, setTotal] = useState();
  const [region, setRegion] = useState(3);
  const [input, setInput] = useState();
  // For loading Screen
  const [loading, setLoading] = useState(true);

  const getPokeRegion = async () => {
    try {
      const data = await getRegion(region);
      const dataId = data.map((poke) => getId(`${poke.pokemon_species.url}`));
      const resultsid = await Promise.all(dataId);
      const promises = resultsid.map((id) => getPokemonDetail(`https://pokeapi.co/api/v2/pokemon/${id}`));
      const results = await Promise.all(promises);
      setPokemons(results);
      setLoading(false);
      setTotal(Math.ceil(data.count / 25));
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    getPokeRegion();
  }, [region]);
  return (
    <div>
      <AuthContextProvider>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/LogIn" element={<LogIn />} />
          <Route path="/SignUp" element={<SignUp />} />
          <Route
            path="/HomeLogged"
            element={(
              <Protected>
                <section className="HomeLoggedAPI">
                  {loading ? <div>Loading...</div>
                    : (
                      <HomeLogged
                        pokemons={pokemons}
                        setRegion={setRegion}
                        total={total}
                        loading={loading}
                      />
                    )}
                </section>
              </Protected>
            )}
          />
          <Route
            path="/Account"
            element={(
              <Protected>
                <Account />
              </Protected>
            )}
          />
          <Route
            path="/Teams"
            element={(
              <Protected>
                <Team setInput={setInput} />
              </Protected>
            )}
          />
          <Route
            path="/CustomTeam"
            element={(
              <Protected>
                <CustomTeam input={input} />
              </Protected>
            )}
          />
        </Routes>
      </AuthContextProvider>
    </div>
  );
}

export default App;
