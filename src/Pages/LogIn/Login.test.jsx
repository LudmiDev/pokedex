import React from 'react';
import { render, screen } from '@testing-library/react';
import Login from './LogIn';

test('renders text', () => {
  render(<Login />);
  const text = screen.getAllByText(/TRAINER CARD/i);
  expect(text).toBeInTheDocument();
});
