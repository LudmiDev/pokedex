import React, { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import 'firebase/compat/auth';
import { signInWithEmailAndPassword } from 'firebase/auth';
import styles from './Login.module.css';
import { UserAuth } from '../../Context/AuthContext';
import { auth } from '../../firebase';

function LogIn() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { googleSignIn, user } = UserAuth();
  const { facebookSignIn } = UserAuth();

  const navigate = useNavigate();

  // Sing In with email and password logic
  const submit = async (e) => {
    e.preventDefault();
    signInWithEmailAndPassword(auth, email, password)
      .then((useCredential) => {
        console.log(useCredential);
      }).catch((error) => {
        console.log(error);
        if (error.code === 'auth/user-not-found') {
          alert('User not found!');
        } else if (error.code === 'auth/wrong-password') {
          alert('Wrong Password!');
        } else if (error.code) {
          alert('Ups! Something went wrong');
        }
      });
  };
  // Handle sign in with google
  const handleGoogleSignIn = async () => {
    try {
      await googleSignIn();
    } catch (error) {
      console.log(error);
    }
  };
    // Handle sign in with fb
  const handleFacebookSignIn = async () => {
    try {
      await facebookSignIn();
    } catch (error) {
      console.log(error);
    }
  };
  // if there's an user navigate to the main page for logged users
  useEffect(() => {
    if (user !== null && user !== undefined) {
      navigate('/HomeLogged');
    }
  }, [user]);

  return (
    <section className={styles.logInSection}>
      <div className={styles.trainerCard}>
        <h1 className="">TRAINER CARD</h1>
        <h1>Log In</h1>
      </div>
      <form className={styles.form}>
        <label htmlFor="email" className="flex flex-col mb-4">
          <h1 className="py-4">EMAIL</h1>
          <input
            type="email"
            placeholder="Enter your email"
            id="email"
            name="email"
            onChange={(e) => setEmail(e.target.value)}
          />
        </label>
        <label htmlFor="password" className="flex flex-col">
          <h1 className="py-4">
            PASSWORD
          </h1>
          <input
            placeholder="Enter your password"
            type="password"
            id="password"
            name="password"
            onChange={(e) => setPassword(e.target.value)}
          />
        </label>
        <div className={styles.formButtons}>
          <div className={styles.firstButton}>
            <button type="button" className={styles.buttonHover} onClick={submit}>
              <div className={styles.pokeImage} />
              SIGN IN
            </button>
          </div>
          <div className="max-w-[240px]">
            <button type="button" className={styles.buttonHover} onClick={handleGoogleSignIn}>
              <div className={styles.pokeImage} />
              GOOGLE
            </button>
          </div>
          <div className="max-w-[240px]">
            <button type="button" className={styles.buttonHover} onClick={handleFacebookSignIn}>
              <div className={styles.pokeImage} />
              FACEBOOK
            </button>
          </div>
        </div>
        <p>
          Oh! You&apos;re new here?
          <Link className="text-red-600" to="/SignUp"> Register here</Link>
        </p>
      </form>
    </section>
  );
}

export default LogIn;
