import React from 'react';
import '../../global/input.css';
import { Link } from 'react-router-dom';
import style from './Home.module.css';

function Home() {
  return (
    <section className={style.home}>
      <div className={style.textGlobe}>
        <h1>
          Hello there fellow trainer.
        </h1>
        <p>
          Would you like to
          <span className="text-red-600">
            <Link to="/LogIn"> Log In </Link>
          </span>
          or
          <span className="text-red-600">
            <Link to="/SignUp"> Sign Up </Link>
          </span>
          ?
        </p>
      </div>
      <div className={style.textGlobeMobile}>
        <h1>
          Hello there fellow
          <br />
          trainer.
        </h1>
        <p>
          Would you like to
          <br />
          <span className="text-red-600">
            <Link to="/LogIn"> Log In </Link>
          </span>
          or
          <span className="text-red-600">
            <Link to="/SignUp"> Sign Up </Link>
          </span>
          ?
        </p>
      </div>

    </section>
  );
}

export default Home;
