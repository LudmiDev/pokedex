import React from 'react';
import { render, screen } from '@testing-library/react';
import Home from './Home';

test('renders text', () => {
  render(<Home />);
  const text = screen.getAllByText(/Hello there fellow trainer./i);
  expect(text).toBeInTheDocument();
});
