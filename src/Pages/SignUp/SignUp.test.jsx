import React from 'react';
import { render, screen } from '@testing-library/react';
import SignUp from './SignUp';

test('renders learn react link', () => {
  render(<SignUp />);
  const text = screen.getAllByLabelText(/EMAIL/i);
  expect(text).toBeInTheDocument();
});
