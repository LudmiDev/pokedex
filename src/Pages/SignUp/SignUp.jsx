import { createUserWithEmailAndPassword } from 'firebase/auth';
import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import {
  getDatabase, ref, set,
} from 'firebase/database';
import { UserAuth } from '../../Context/AuthContext';
import styles from './SignUp.module.css';
import { auth } from '../../firebase';

function SignUp() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { googleSignIn, user } = UserAuth();
  const { facebookSignIn, userfb } = UserAuth();

  const navigate = useNavigate();
  // Set data to database
  async function writeUserData(mail, uid) {
    const db = getDatabase();
    await set(ref(db, `users/${uid}`), {
      mail,
    });
  }

  // Create user up logic
  const signUp = async (e) => {
    e.preventDefault();
    await createUserWithEmailAndPassword(auth, email, password)
      .then((useCredential) => {
        navigate('/HomeLogged');
        writeUserData(email, useCredential.user.uid);
      })
      .catch((error) => {
        if (error.code === 'auth/invalid-email') {
          alert('Invalid mail!');
        } else if (error.code === 'auth/weak-password') {
          alert('Weak Password!');
        } else if (error.code === 'auth/email-alredy-in-use') {
          alert('Email alredy in use');
        } else if (error.code) {
          alert('Ups! Something went wrong');
        }
      });
  };
  // Google Handle sign Up
  const handleGoogleSignUp = async () => {
    try {
      await googleSignIn();
    } catch (error) {
      console.log(error);
    }
  };
  // Facebook handle sign up
  const handleFacebookSignUp = async () => {
    try {
      await facebookSignIn();
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (user !== null && user !== undefined) {
      navigate('/HomeLogged');
      writeUserData(user.email, user.uid);
    }
  }, [user]);
  useEffect(() => {
    if (userfb !== null && userfb !== undefined) {
      navigate('/HomeLogged');
      writeUserData(userfb.email, userfb.uid);
    }
  }, [userfb]);

  return (
    <section className={styles.signUpSection}>
      <div className={styles.trainerCard}>
        <h1 className="">TRAINER CARD</h1>
        <h1>Register</h1>
      </div>
      <form className={styles.form}>
        <label htmlFor="email" className="flex flex-col mb-4">
          <h1 className="py-4">EMAIL</h1>
          <input
            type="email"
            placeholder="Enter your email"
            id="email"
            name="email"
            onChange={(e) => setEmail(e.target.value)}
          />
        </label>
        <label htmlFor="password" className="flex flex-col">
          <h1 className="py-4">
            PASSWORD
          </h1>
          <input
            placeholder="Enter your password"
            type="password"
            id="password"
            name="password"
            onChange={(e) => setPassword(e.target.value)}
          />
        </label>
        <div className={styles.formButtons}>
          <div className={styles.firstButton}>
            <button type="button" className={styles.buttonHover} onClick={signUp}>
              <div className={styles.pokeImage} />
              SIGN IN
            </button>
          </div>
          <div className="max-w-[240px]">
            <button type="button" className={styles.buttonHover} onClick={handleGoogleSignUp}>
              <div className={styles.pokeImage} />
              GOOGLE
            </button>
          </div>
          <div className="max-w-[240px]">
            <button type="button" className={styles.buttonHover} onClick={handleFacebookSignUp}>
              <div className={styles.pokeImage} />
              FACEBOOK
            </button>
          </div>
        </div>
      </form>
    </section>
  );
}
export default SignUp;
