/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable react/prop-types */
import React, { useEffect, useState, useContext } from 'react';
import '../../global/input.css';
import Navbar from '../../Components/Navbar/Navbar';
import searchPokemon from './api';
import PokemonCard from '../../Components/PokemonCard/PokemonCard';
import audio from '../../Assets/pokemon_a_button_gba.mp3';
import styles from './HomeLogged.module.css';
import { firebaseGET } from '../../firebaseReq/firebaseGET';
import AuthContext from '../../Context/AuthContext';
import PokemonCardSearch from '../../Components/PokemonCard/PokemonCardSearch';

function HomeRegion(props) {
  const [search, setSearch] = useState('');
  const [pokemon, setPokemon] = useState('');
  const [teams, setTeams] = useState([]);
  const [menu, setMenu] = useState(false);

  const Plim = new Audio(audio);

  // Onclick Show menu, play animation and reproduce sound
  function showMenu() {
    setMenu(!menu);
    Plim.play();
  }

  const { user } = useContext(AuthContext);

  const {
    pokemons, setRegion,
  } = props;

  // Target the select value, and send it to getRegion
  function passData(e) {
    setRegion(parseInt(e.target.value, 10));
    showMenu();
  }

  // target the input value, and put it in setSearch
  const handlePokeSearch = (e) => {
    setSearch(e.target.value);
  };

  // Grab the data from API, compare with input data and return desired value
  const submitSearch = async (e) => {
    e.preventDefault();
    if (search === '') { // This prevents the web from crashing if the input has no value
      return;
    }
    const data = await searchPokemon(search);
    setPokemon(data);
  };
  useEffect(() => {
    if (user !== null) {
      firebaseGET(`users/${user.uid}/`, 'teams')
        .then((response) => {
          if (response) {
            setTeams(Object.values(response));
          }
        });
    }
  }, [user]);

  return (
    <>
      <Navbar />
      <section className={styles.bigWrap}>
        <main className={styles.home}>
          <button type="button" className={styles.buttonRegion} onClick={showMenu}>Sort By Region</button>
          {menu
              && (
              <nav className="flex flex-col absolute top-40">
                <button type="button" value={2} onClick={passData} className={styles.buttonNav}>Kanto</button>
                <button type="button" value={3} onClick={passData} className={styles.buttonNav}>Johto</button>
                <button type="button" value={15} onClick={passData} className={styles.buttonNav}>Hoenn</button>
                <button type="button" value={5} onClick={passData} className={styles.buttonNav}>Sinnoh</button>
                <button type="button" value={8} onClick={passData} className={styles.buttonNav}>Teselia/Unova</button>
                <button type="button" value={12} onClick={passData} className={styles.buttonNav}>Kalos</button>
                <button type="button" value={16} onClick={passData} className={styles.buttonNav}>Alola </button>
                <button type="button" value={27} onClick={passData} className={styles.buttonNav}>Galar</button>
              </nav>
              )}
          <form onSubmit={submitSearch} className={styles.form}>
            <input placeholder="Search Pokemon" onChange={handlePokeSearch} />
            <button type="button" className={styles.buttonForm} onClick={submitSearch}>Search</button>
          </form>
        </main>
        <div>
          {pokemon
            && (
            <div>
              <PokemonCardSearch pokemon={pokemon} />
            </div>
            )}
        </div>
        <div className={styles.pokediv}>
          {pokemons.map((ele, idx) => (
            <PokemonCard ele={ele} idx={idx} key={ele.name} teams={teams} />
          ))}
        </div>
      </section>

    </>
  );
}

export default HomeRegion;
