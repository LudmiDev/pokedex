import React from 'react';
import { render, screen } from '@testing-library/react';
import HomeLogged from './HomeLogged';

test('renders learn react link', () => {
  render(<HomeLogged />);
  const text = screen.getAllByText(/Kanto/i);
  expect(text).toBeInTheDocument();
});
