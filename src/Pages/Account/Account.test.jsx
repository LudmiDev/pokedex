import React from 'react';
import { render, screen } from '@testing-library/react';
import Account from './Account';

test('renders text', () => {
  render(<Account />);
  const text = screen.getAllByText(/Welcome/i);
  expect(text).toBeInTheDocument();
});
