/* eslint-disable no-trailing-spaces */
import React from 'react';
import { UserAuth } from '../../Context/AuthContext';
import Navbar from '../../Components/Navbar/Navbar';
import styles from './Account.module.css';

function Account() {
  const { logOut, user } = UserAuth();

  const handleSignOut = async () => {
    try {
      await logOut();
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <Navbar />
      <section className={styles.Account}>
        <h1 className="text-center text-2xl font-bold pt-12">Account</h1>
        <div>
          <p>
            Welcome 
          </p>
          <p> 
            {user?.displayName || 'Fellow trainer'}
          </p>
        </div>
        <button type="button" onClick={handleSignOut} className="border py-2 px-5 mt-10">
          Log Out
        </button>
      </section>
    </>
  );
}

export default Account;
