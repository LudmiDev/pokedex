import React, { useState } from 'react';
import '../../global/input.css';
import { Link } from 'react-router-dom';
import styles from './Navbar.module.css';
import audio from '../../Assets/pokemon_a_button_gba.mp3';
import pokeTitle from '../../Assets/pokeTitle.png';

function Navbar() {
  const [menu, setMenu] = useState(false);

  const Plim = new Audio(audio);

  // Onclick Show menu, play animation and reproduce sound
  function animation() {
    setMenu(!menu);
    Plim.play();
  }
  return (
    <>
      <nav className={styles.navbar}>
        <a href="/HomeLogged" className="text-center text-2xl font-bold">
          <img src={pokeTitle} alt="pokeapiTitle" />
        </a>
        <div>
          <button type="button" className={styles.container} onClick={animation}>
            <svg xmlns="http://www.w3.org/2000/svg" width="150" height="150" viewBox="0 0 200 200">
              <g strokeWidth="6.5" strokeLinecap="round">
                <path
                  d="M72 82.286h28.75"
                  fill="#009100"
                  fillRule="evenodd"
                  stroke="#fff"
                />
                <path
                  d="M100.75 103.714l72.482-.143c.043 39.398-32.284 71.434-72.16 71.434-39.878 0-72.204-32.036-72.204-71.554"
                  fill="none"
                  stroke="#fff"
                />
                <path
                  d="M72 125.143h28.75"
                  fill="#009100"
                  fillRule="evenodd"
                  stroke="#fff"
                />
                <path
                  d="M100.75 103.714l-71.908-.143c.026-39.638 32.352-71.674 72.23-71.674 39.876 0 72.203 32.036 72.203 71.554"
                  fill="none"
                  stroke="#fff"
                />
                <path
                  d="M100.75 82.286h28.75"
                  fill="#009100"
                  fillRule="evenodd"
                  stroke="#fff"
                />
                <path
                  d="M100.75 125.143h28.75"
                  fill="#009100"
                  fillRule="evenodd"
                  stroke="#fff"
                />
              </g>
            </svg>
          </button>
        </div>
      </nav>
      {menu
          && (
          <nav className={styles.pokeNav}>
            <ul>
              <li>
                <div className={styles.arrowleft} />
                <Link to="/Account">
                  <h1>Account</h1>
                </Link>
              </li>
              <li>
                <div className={styles.arrowleft} />
                <Link to="/Teams"><h1>Teams</h1></Link>
              </li>
              <li>
                <div className={styles.arrowleft} />
                <Link to="/HomeLogged"><h1>Pokedex</h1></Link>
              </li>
            </ul>
          </nav>
          )}
    </>
  );
}

export default Navbar;
