/* eslint-disable max-len */
/* eslint-disable react/prop-types */
import React, { useContext, useEffect, useState } from 'react';
import {
  getDatabase, ref, remove,
} from 'firebase/database';
import AuthContext from '../../Context/AuthContext';
import styles from './CustomTeam.module.css';
import { firebaseGET } from '../../firebaseReq/firebaseGET';

import Navbar from '../Navbar/Navbar';

function CustomTeam(props) {
  const [pokeData, setPokeData] = useState();
  // const [selectedPoke, setSelectedPoke] = useState();
  const { user } = useContext(AuthContext);

  const {
    input,
  } = props;

  useEffect(() => {
    if (user !== null) {
      firebaseGET(`users/${user.uid}/`, `teams/${input}`)
        .then((response) => {
          if (response) {
            setPokeData(Object.values(response));
          }
        });
    }
  }, [user]);
  function DeletePoke(params) {
    const db = getDatabase();
    remove(ref(db, `/users/${user.uid}/teams/${input}/${params}`));
    window.location.replace('/Teams');
  }

  return (
    <>
      <Navbar />
      <div>
        <section className={styles.wrap}>
          <main className={styles.inside}>
            {pokeData === undefined ? <p>Loading pokemon...</p> : pokeData.map((pokemon) => (
              <div className={styles.block} key={pokemon.id}>
                <div className={styles.pokeimage}>
                  <img src={pokemon.image} alt="pokemon" />
                </div>
                <div className="flex justify-between w-4/5">
                  <div className="flex flex-col">
                    <h1>
                      {pokemon.pokeName}
                    </h1>
                    <h2>{pokemon.id}</h2>
                    <p>{pokemon.type}</p>
                  </div>
                  <div className="flex">
                    <button
                      className={styles.releasebutton}
                      type="button"
                      onClick={() => DeletePoke(pokemon.id)}
                    >
                      Release
                    </button>
                  </div>
                </div>
              </div>
            ))}
          </main>
        </section>

      </div>

    </>
  );
}

export default CustomTeam;
