/* eslint-disable react/prop-types */
import React, { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import {
  getDatabase, ref, remove,
} from 'firebase/database';
import Navbar from '../Navbar/Navbar';
import styles from './Teams.module.css';
import { firebaseGET } from '../../firebaseReq/firebaseGET';
import AuthContext from '../../Context/AuthContext';

function Team(props) {
  const [teamData, setTeamData] = useState();
  const { user } = useContext(AuthContext);
  const {
    setInput,
  } = props;

  function input(e) {
    setInput(e.target.value);
  }
  function DeleteTeam(e) {
    const db = getDatabase();
    remove(ref(db, `/users/${user.uid}/teams/${e.target.value}`));
    window.location.replace('/Teams');
  }

  useEffect(() => {
    if (user !== null) {
      firebaseGET(`users/${user.uid}/`, 'teams/')
        .then((response) => {
          if (response) {
            const teams = Object.entries(response);
            setTeamData(teams.map((team) => team[0]));
          }
        });
    }
  }, [user]);
  return (
    <>
      <Navbar />
      <section className={styles.wrap}>
        <main className={styles.inside}>
          {teamData ? teamData.map((team) => (
            <div className="w-full flex justify-between">
              <Link to="/CustomTeam" key={Math.random()} className="w-full flex justify-center">
                <button type="button" key={Math.random()} onClick={input} value={team} className={styles.block} id={team}>
                  <div className={styles.pokeImage} />
                  <h1 key={Math.random()}>
                    {team}
                  </h1>
                </button>
              </Link>
              <button
                type="button"
                className={styles.deleteButton}
                onClick={DeleteTeam}
                value={team}
              >
                Delete
                {' '}
                {team}
              </button>
            </div>
          )) : null}

        </main>
      </section>

    </>
  );
}

export default Team;
