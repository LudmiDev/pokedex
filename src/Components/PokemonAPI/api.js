/* eslint-disable consistent-return */
// get pokemons by region
export const getRegion = async (param) => {
  try {
    const region = param;
    const newurl = `https://pokeapi.co/api/v2/pokedex/${region}`;
    const response = await fetch(newurl);
    const data = await response.json();
    return (data.pokemon_entries);
  } catch (error) {
    console.log(error);
  }
};

// This fetch gets the detail of every pokemon from the URL above
export const getPokemonDetail = async (url) => {
  try {
    const response = await fetch(url);
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
};

export const getId = async (url) => {
  try {
    const response = await fetch(url);
    const data = await response.json();
    return (data.id);
  } catch (error) {
    console.log(error);
  }
};
