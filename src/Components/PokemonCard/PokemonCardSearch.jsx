import React, { useState, useContext } from 'react';
import {
  getDatabase, ref, set,
} from 'firebase/database';
import PropTypes from 'prop-types';

import AuthContext from '../../Context/AuthContext';

import styles from './PokemonCard.module.css';

function PokemonCardSearch({ pokemon }) {
  const [menuIsOpen, setMenuIsOpen] = useState(false);

  function handlePokemonData() {
    const menuDiv = document.getElementById(pokemon.id);
    if (menuIsOpen) {
      menuDiv.classList.replace(styles.teamsMenu, styles.teamsMenuHide);
      setMenuIsOpen(false);
    } else {
      menuDiv.classList.replace(styles.teamsMenuHide, styles.teamsMenu);
      setMenuIsOpen(true);
    }
  }
  const { user } = useContext(AuthContext);
  function handleAddA() {
    const db = getDatabase();
    console.log(pokemon.sprites.front_default);
    set(
      ref(db, `/users/${user.uid}/teams/team1/${pokemon.id}`),
      {
        pokeName: pokemon.name,
        image: pokemon.sprites.front_default,
        id: pokemon.id,
        type: pokemon.types.map((type) => type.type.name),
      },
    );
  }

  function handleAddB() {
    const db = getDatabase();
    set(ref(db, `/users/${user.uid}/teams/team2/${pokemon.id}`), {
      pokeName: pokemon.name,
      image: pokemon.sprites.front_default,
      id: pokemon.id,
      type: pokemon.types.map((type) => type.type.name),
    });
  }
  function handleAddC() {
    const db = getDatabase();
    set(ref(db, `/users/${user.uid}/teams/team3/${pokemon.id}`), {
      pokeName: pokemon.name,
      image: pokemon.sprites.front_default,
      id: pokemon.id,
      type: pokemon.types.map((type) => type.type.name),
    });
  }

  return (
    <article className={styles.pokeCard} key={pokemon.id}>
      <div className={styles.image}>
        <img src={pokemon.sprites.front_default} alt={pokemon.name} />
      </div>
      <div className={styles.nameandid}>
        <h1>{pokemon.name}</h1>
        <h4>{pokemon.id}</h4>
      </div>

      <div>
        {pokemon.types.map((type) => <div key={type.type.name}>{type.type.name}</div>)}
      </div>

      <button type="button" onClick={handlePokemonData}>Add to team</button>
      <div className={styles.teamsMenuHide} id={pokemon.id}>
        <button type="button" onClick={handleAddA}>
          Team A
        </button>
        <button type="button" onClick={handleAddB}>
          Team B
        </button>
        <button type="button" onClick={handleAddC}>
          Team C
        </button>
      </div>
    </article>
  );
}
PokemonCardSearch.propTypes = {
  pokemon: PropTypes.node.isRequired,
};

export default PokemonCardSearch;
