import React, { useState, useContext, useEffect } from 'react';
import {
  getDatabase, ref, set,
} from 'firebase/database';
import AuthContext from '../../Context/AuthContext';
import successAudio from '../../Assets/pokemon_item_pickup.mp3';

import styles from './PokemonCard.module.css';
import { firebaseGET } from '../../firebaseReq/firebaseGET';

function pokemonCard({ ele }) {
  const [menuIsOpen, setMenuIsOpen] = useState(false);
  const [menu, setMenu] = useState(false);
  const [success, setSuccess] = useState(false);
  const [newTeam, setNewTeam] = useState([]);
  const [teamData, setTeamData] = useState();

  const Plim = new Audio(successAudio);

  function handlePokemonData() {
    const menuDiv = document.getElementById(ele.id);
    if (menuIsOpen) {
      menuDiv.classList.replace(styles.teamsMenu, styles.teamsMenuHide);
      setMenuIsOpen(false);
    } else {
      menuDiv.classList.replace(styles.teamsMenuHide, styles.teamsMenu);
      setMenuIsOpen(true);
    }
  }
  function showMenu() {
    setSuccess(!success);
    Plim.play();
  }
  const { user } = useContext(AuthContext);

  // Add pokemon to custom team
  function handleNewTeam(e) {
    const db = getDatabase();
    set(ref(db, `/users/${user.uid}/teams/${e.target.value}/${ele.id}`), {
      pokeName: ele.name,
      image: ele.sprites.front_default,
      id: ele.id,
      type: ele.types.map((type) => type.type.name),
    });
    showMenu();
  }
  // Add new Team
  function addNewTeam() {
    const db = getDatabase();
    set(ref(db, `/users/${user.uid}/teams/${newTeam}/${ele.id}`), {
      pokeName: ele.name,
      image: ele.sprites.front_default,
      id: ele.id,
      type: ele.types.map((type) => type.type.name),
    });
    showMenu();
  }
  // Get the custom teams
  useEffect(() => {
    if (user !== null) {
      firebaseGET(`users/${user.uid}/`, 'teams/')
        .then((response) => {
          if (response) {
            const teams = Object.entries(response);
            setTeamData(teams.map((team) => team[0]));
          }
        });
    }
  }, [user]);
  return (
    <article className={styles.pokeCard} key={ele.id}>
      <div className={styles.image}>
        <img src={ele.sprites.front_default} alt={ele.name} />
      </div>
      <div className={styles.nameandid}>
        <h1>{ele.name}</h1>
        <h4>{ele.id}</h4>
      </div>
      { success && (
      <div className={styles.teamsSuccess}>
        <button type="button" onClick={() => setSuccess(!success)}>
          X
        </button>
        <h1>
          Your pokemon
          {' '}
          <span className="text-red-600">{ele.name}</span>
          {' '}
          was added to your team!
        </h1>
        <img src={ele.sprites.front_default} alt={ele.name} />
      </div>
      )}
      <div>
        {ele.types.map((type) => <div key={type.type.name}>{type.type.name}</div>)}
      </div>

      <button type="button" onClick={handlePokemonData}>Add to team</button>
      <div className={styles.teamsMenuHide} id={ele.id}>
        { teamData ? teamData.map((team) => (
          <button type="button" value={team} id={team} key={Math.random()} onClick={handleNewTeam}>
            {team}
          </button>
        )) : null}
        <button type="button" onClick={() => setMenu(!menu)}>
          New Team
        </button>
        { menu && (
        <form className="w-full flex justify-center">
          <input
            className="w-1/5 border-2 border-rose-700 mr-6"
            onChange={(e) => setNewTeam([e.target.value])}
          />
          <button type="button" onClick={addNewTeam}>Add new team</button>
        </form>
        )}
      </div>
    </article>
  );
}

export default pokemonCard;
