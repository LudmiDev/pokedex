# Pokedex

![alt text](https://res.cloudinary.com/dweiaqd6l/image/upload/v1671812637/imagen_2022-12-23_132357277_d7xhgi.png)

## Welcome!

Thank you for being here, this project was an absolute disaster, so I apology in advance for all the spagetti you may find.

## You can watch the project fully deployed [here](https://pokedex-app-ludmidev-elaniin.vercel.app)

## How Pokedex is organized
This project is organized into folders, which inside have the functionality of the component itself and its style. 
Is mainly divided in **Components** and **Pages** then you have  firebase config & a folder of firebase actions outside, App, global styles, etc.


![alt text](https://res.cloudinary.com/dweiaqd6l/image/upload/v1671813059/imagen_2022-12-23_133059104_rsfkdp.png)

I hope it's clear to you


## Getting started

### Prerequisites

- Have a text or code editor
- Node: any 12.x version starting with v12.0.0 or greater
- Git
- NPM or Yarn (npm preferred)

### Installation 
On GitHub.com, navigate to the main page of the repository. Above the list of files, click Code.

![alt text](https://docs.github.com/assets/cb-20363/images/help/repository/code-button.png)

Copy the URL for the repository, open Git Bash. Change the current working directory to the location where you want the cloned directory.

```
$cd desktop

```
Type git clone, and then paste the URL you copied earlier.
```
$ git clone https://gitlab.com/LudmiDev/pokedex.git

```
Press Enter to create your local clone.

```
$ git clone https://gitlab.com/LudmiDev/pokedex.git
> Cloning into `Spoon-Knife`...
> remote: Counting objects: 10, done.
> remote: Compressing objects: 100% (8/8), done.
> remove: Total 10 (delta 1), reused 10 (delta 1)
> Unpacking objects: 100% (10/10), done.
```
Then, put npm install, to install all the dependencies

```
npm install
```
